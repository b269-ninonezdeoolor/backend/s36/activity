// Contains instructions on HOW your API will perform its intended tasks
// All the operations it can do will be placed in this file
const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () =>
{
	return Task.find({}).then(result =>
	{
		return result;
	});
};

// Controller function for creating a task
module.exports.createTask = (requestBody) =>
{
	let newTask = new Task({
		name: requestBody.name
	});

	return newTask.save().then((task, error) =>
	{
		if(error)
		{
			console.log(error);
			return false;
		}
		else
		{
			return task;
		}
	});
};

// Controller function for deleting a task
module.exports.deleteTask = (taskId) =>
{
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>
	{
		if(err)
		{
			console.log(err);
			return false;
		}
		else
		{
			return "Deleted Task!";
		}
	});
};

// [SECTION] ACTIVITY
// Controller function for getting a specific task
module.exports.getTaskById = (taskId) =>
{
	return Task.findById(taskId).then(result =>
	{
		return result;
	});
};

// Controller function for updating the task status to complete
module.exports.updateStatusToComplete = (taskId) =>
{
	return Task.findByIdAndUpdate(taskId, { status: "complete" }, { new: true }).then((result) =>
	{
		return result;
	});
};