// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes define in the "taskRoute.js" file
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
// Connecting to MongoDB Atlas START
mongoose.connect("mongodb+srv://ninonezdeoolor11:admin123@zuitt-bootcamp.sul2u9q.mongodb.net/s36?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database.`));

// Add task route
// Allows all the task routes create in the "taskRoute.js" file to use the "/tasks" route
app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}...`));